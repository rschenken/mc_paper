% The file generates random realizations of the power consumption
%   author: Ulrich Roemer
%   description: 
%       -the Cholesky or eigendecomposition can be constructed calling
%       perform_decomp_cov.m; results are stored in
%       decomp_hist_A320/B772.mat
%       -call_stochastic_model.m generates a sample based on the covariance
%       decomposition
%       -the file plots the results

% set seed to make results reproducible
rng('default')    

% load data 
%str_id = 'hist_A320';    
str_id = 'hist_B772';   
load(strcat('decomp_',str_id,'.mat'))

[f,f_orig] = call_stochastic_model(decomp_dat,5000);    % create new samples; use f in Monte Carlo

% Plotting
verbose = 1;    % flag for plotting

dt = 1:length(f(1,:));
load(strcat('raw_data_',str_id,'.mat'))

% adapt data for B772; needed to homogenize length for all airplane types
if strcmp(str_id, 'hist_B772')
    x_max = length(X(1,:));
    x_sub = 1:1:x_max;
    X = X(:,x_sub);
end

% compute moments
moms = zeros(4,length(dt));
moms_generated = zeros(4,length(dt));
for i = 1:length(dt)
    moms(1,i) =  mean(X(:,i));
    moms_generated(1,i) =  mean(f_orig(:,i));
    moms(2,i) =  std(X(:,i));
    moms_generated(2,i) =  std(f_orig(:,i));    
end

if verbose     
    
    % computer generated samples    
    figure(1)    
    plot(dt,f(1,:),dt,f(2,:),dt,f(3,:),dt,f(4,:),dt,f(5,:))
    title('computer generated data samples')

    % original samples
    figure(2)
    plot(dt,X(10,:),dt,X(23,:),dt,X(1,:),dt,X(100,:),dt,X(70,:))
    title('original data samples')
    
    % mean value and standard deviation
    figure(3) 
    subplot(2,2,1)
    plot(dt,moms(1,:),'-',dt,moms_generated(1,:),'--')
    ylim([0,6000])
    legend('original','decomposition','Location','NorthEast')
    title('mean values')
    xlabel('P [W]')
    ylabel('t [s]')
    subplot(2,2,2)
    plot(dt,moms(2,:),'-',dt,moms_generated(2,:),'--')
    legend('original','decomposition')
    xlabel('P [W]')
    ylabel('t [s]')
    title('standard deviations')
    
    % mean value and standard deviation errors
    figure(3) 
    subplot(2,2,3)
    plot(dt,abs(moms(1,:)-moms_generated(1,:)))
    %legend('error')
    xlabel('P [W]')
    ylabel('t [s]')
    title('mean value error')
    subplot(2,2,4)
    plot(dt,abs(moms(2,:)-moms_generated(2,:)))
    %legend('error')
    xlabel('P [W]')
    ylabel('t [s]')
    title('standard deviation error')
        
end


