## Code to produce numerical results of the paper "Design of Fuel Cell Systems for Aviation:Representative Mission Profiles and Sensitivity Analyses"

The code can be used to generate a stochastic model of random flight profiles (power consumptions) and perform a Monte Carlo and Sensitivity Analysis of the design of Fuel Cell systems (and hybrid Fuel Cell - Battery) systems for aviation.

---

## Section 2.1 Flight Mission Profiles

The results of the flight simulation are stored in 

- 20182508_B772_CTred.mat
- 20182508_A320_CTred.mat

---

## Section 2.3 Stochastic Model

The files to produce the results of this section are 

- preprocess_flight_profile_data.m
- perform_decomp_cov.m
- call_stochastic_model.m
- runme.m

---

## Sections 2.4, 2.5 Sensitivity and Monte Carlo Analysis

The files to produce the results of this section are 

- runme_TK181121_Cholesky.m
- runme_TK181121_sensitivity_analysis.m

---


