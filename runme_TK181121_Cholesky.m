% Function to be executed; load results from Cholesky decomposition / PCA and generate random samples

close all;clear all;

% set seed to make results reproducable
rng('default')    

% load data 
% str_id = 'hist_A320';    
str_id = 'hist_B772';   
load(strcat('decomp_',str_id,'.mat'))

n_s = 10000; % number of samples
[f,f_orig] = call_stochastic_model(decomp_dat,n_s);    % create new samples; use f in Monte Carlo

% Plotting
verbose = 1;    % flag for plotting

dt = 1:length(f(1,:));
load(strcat('raw_data_',str_id,'.mat'))

if strcmp(str_id, 'hist_B772')
    x_max = length(X(1,:));
    x_sub = 1:1:x_max;
    X = X(:,x_sub);
end

moms = zeros(4,length(dt));
moms_generated = zeros(4,length(dt));
for i = 1:length(dt)
    moms(1,i) =  mean(X(:,i));
    moms_generated(1,i) =  mean(f_orig(:,i));
    moms(2,i) =  std(X(:,i));
    moms_generated(2,i) =  std(f_orig(:,i));
    moms(3,i) =  skewness(X(:,i));
    moms_generated(3,i) =  skewness(f_orig(:,i));
    moms(4,i) =  kurtosis(X(:,i));
    moms_generated(4,i) =  kurtosis(f_orig(:,i));
end

if verbose     
    
    % computer generated samples    
    figure(1)    
    plot(dt,f(1,:),dt,f(2,:),dt,f(3,:),dt,f(4,:),dt,f(5,:))
    title('computer generated data samples')

    % original samples
    figure(2)
    plot(dt,X(10,:),dt,X(23,:),dt,X(1,:),dt,X(100,:),dt,X(70,:))
    title('original data samples')
    
    % mean value and standard deviation
    figure(3) 
    subplot(2,2,1)
    plot(dt,moms(1,:),'-',dt,moms_generated(1,:),'--')
    ylim([0,6000])
    legend('original','decomposition','Location','NorthEast')
    title('mean values')
    xlabel('P [W]')
    ylabel('t [s]')
    subplot(2,2,2)
    plot(dt,moms(2,:),'-',dt,moms_generated(2,:),'--')
    legend('original','decomposition')
    xlabel('P [W]')
    ylabel('t [s]')
    title('standard deviations')
    
    % mean value and standard deviation errors
    figure(3) 
    subplot(2,2,3)
    plot(dt,abs(moms(1,:)-moms_generated(1,:)))
    %legend('error')
    xlabel('P [W]')
    ylabel('t [s]')
    title('mean value error')
    subplot(2,2,4)
    plot(dt,abs(moms(2,:)-moms_generated(2,:)))
    %legend('error')
    xlabel('P [W]')
    ylabel('t [s]')
    title('standard deviation error')
        

end


%% Start Thomas+Sebastians FC+Tank Models
f=f';
load '170504_Reference_case.mat' eta P_nd;

%% parameters 
% material parameters
p.rho_H2 = 71281;     % [g/m�] density H2 @ 1013hPa, 20K; NIST webbook
p.E_sp_H2   = 33.3*3600; % [Ws/g] energy density H2, 33.3 kWh/kg
p.m_sp_t = 75000;     % [g/m�] spec. weight tank, estimate based on Linde cryogenic tanks
% fuel cell parameters
p.P_sp_FC = 1.6;       % [W/g] power density FC stack, based on Toyota Mirai, 2kW/kg
% p.P_sp_FC = 8.000;      % [W/g] power density FC stack, future estimate from Kadyk,Energies 2017

% battery parameters
p.eta_bat = 0.8;
p.E_spBat = 0.8*3600; % [Ws/g] energy density battery; 
p.PE_Bat = 2/3600; % [kW/kWs]

% % battery parameters from Nagata16: all solid state LiS battery
% p.E_spBat = 0.5*3600; % [Ws/g] energy density battery; est: 0.5kWh/kg von Astrid/JanMueller18 Energies 
% p.PE_Bat = 2/3600; % [kW/kWs]

% alt: supercap parameters (estimates from internet search)
% p.E_spBat = 0.05*3600; % [Ws/g] 
% p.PE_Bat = 10/0.05/3600; % [kW/kWs]

% gas turbine parameters 
p.eta_gtem = 0.35; % combined efficiencies of gas turbine and electric engine, eta_gt/eta_em

%% create power vectors (x-axes of histogram)
Pv_cker=[1:length(f(:,1))]'; % chemical power of kerosene (~kerosene flow rate)

%% include for turbine efficiency, i.e. calculate mech. power

Pv_el=Pv_cker*p.eta_gtem; % required electrical power

%% include FC efficiency, i.e. calculate hydrogen flow rate/power

Pvnd_el = Pv_el./max(Pv_el);

% create fuel cell efficiency curve with the same Power vector (x-axis)
for lv=1:length(Pvnd_el)
 etav(lv,1)= eta(find(P_nd>=Pvnd_el(lv),1));
end

figure(1);
plot(Pvnd_el,etav);

Pv_cH2=Pv_el./etav; % reqired chemical power of hydrogen (~hydrogen flow rate)



%% include fuel cell efficiency

mf_H2=Pv_cH2./p.E_sp_H2 * 10^6; % hydrogen mass flow in g/s

figure(2); % die ersten drei Datens�tze werden geplottet:
subplot(4,1,1);plot(Pv_cker,f(:,1:3));xlabel('P_{kerosene,req}');
subplot(4,1,2);plot(Pv_el,f(:,1:3));xlabel('P_{el,req}');
subplot(4,1,3);plot(Pv_cH2,f(:,1:3));xlabel('P_{H2,req}');
subplot(4,1,4);plot(mf_H2,f(:,1:3));xlabel('required hydrogen mass flow / (t/s)');

% calculate energy and H2 mass required by the fuel cell
E_reqel = sum(f.* (Pv_el*ones(1,n_s)),1);
E_reqH2 = sum(f.* (Pv_cH2*ones(1,n_s)),1);
m_H2 = E_reqH2./p.E_sp_H2 * 10^3; % hydrogen mass in kg

figure(3);
subplot(3,1,1)
plot(E_reqel,'x');xlabel('sample #');ylabel('required electrical energy / MWs');
subplot(3,1,2)
plot(E_reqH2,'x');xlabel('sample #');ylabel('required hydrogen energy / MWs');
subplot(3,1,3)
plot(m_H2,'x');xlabel('sample #');ylabel('required hydrogen mass / t');

figure(4);
histogram(m_H2,'Normalization','probability');
xlabel('hydrogen mass / t');ylabel('rel. number of observations');


%% determine max. required power, fuel cell mass and Tank System mass:
p.Aircraft_Characterisitic = zeros(n_s,3); % first column: hydrogen mass, second column: massflow LH2 at PMin, third column: Tank mass
p.TankTechnology = 2; % Cryogenic Tank
p.TankDesign = 1; % cylindrical design
p.TankElongation = 0.7; % Elongation
p.WallMaterial = 11; 
p.InsulationMaterial = 2;
p.InsulationHeatflowshare_PMin = 1;
p.HydrogenDesignPressure = 1.2;
p.HydrogenStartTemperature = 23; 
p.WallMinTempereature = 19;
p.Additional_Insulation = 0;
[~, ~, LH2_density] = EOS_Leachman(1, 23, 10, p.HydrogenDesignPressure * 100000, 0, 'para');
[~, ~, GH2_density] = EOS_Leachman(1, 24, 10, p.HydrogenDesignPressure * 100000, 0, 'para');

for lv2=1:n_s
  Pmax_el(lv2)=Pv_el(find(f(:,lv2),1,'last'));
%   mfH2_Pmax(lv2) = mf_H2(Pmax_s(lv2));
%   Pmin_s(lv2)=find(f(:,lv2),1,'first');
  mfH2_Pmin(lv2) = mf_H2(find(f(:,lv2),1,'first'));
  hyb(:,lv2)=Pv_el./Pmax_el(lv2); %degree of hybridization, for next section below
  [~,m_T(lv2),~,~] = Tank_submodel(p.TankDesign,...
  p.TankElongation, p.WallMaterial, p.InsulationMaterial,p.InsulationHeatflowshare_PMin,...
  mfH2_Pmin(lv2), m_H2(lv2),...
  p.HydrogenDesignPressure, p.HydrogenStartTemperature, p.WallMinTempereature,...
  p.Additional_Insulation, LH2_density, GH2_density);
end

figure(5);
histogram(m_T,'Normalization','probability');
xlabel('tank mass / kg');ylabel('rel. number of observations');

figure(6);
plot(Pmax_el,'x');xlabel('sample #'),ylabel('max. elec. Power ');

figure(7);
histogram(Pmax_el./p.P_sp_FC,'Normalization','probability');xlabel('fuel cell mass / t');

%% hybridization
%% Now let's look at undersizing the fuel cell and complimenting it with a second energy storage
% idea: cut off the "tail" of the histogram and provide that part by a battery
% i.e. "undersize" the fuel cell

% determine FC and battery mass for each degree of hybridization
for lv3=1:length(Pv_el)
  E_req_fc(lv3,:)   = sum(f(1:lv3,:)    .* (Pv_cH2(1:lv3)*ones(1,n_s)),1)*1e6;  
  E_req_batE(lv3,:) = sum(f(lv3+1:end,:).* (Pv_el(lv3+1:length(Pv_el))*ones(1,n_s)),1)*1e6/p.eta_bat; %required battery capacity based on required energy capacity   
  E_req_batP(lv3,:) = (Pmax_el-Pv_el(lv3))*1e6/p.PE_Bat; %required battery capacity based on required power capacity
end;

% the required battery capacity is max(E_req_batE,E_req_batP)
E_req_bat=E_req_batE; %first fill matrix with E_req_batE values
E_req_bat(E_req_batP>E_req_batE)=E_req_batP(E_req_batP>E_req_batE); % replace value if E_req_batP is higher

m_H2u = E_req_fc./p.E_sp_H2;
m_Batu = E_req_bat./p.E_spBat;

n_hyb = length(Pv_el); %need this to run the nested for loop inside the parfor loop below
m_FCu = zeros(length(Pv_el),n_s);
m_Tu = zeros(length(Pv_el),n_s);

%% Use tank model
% parfor lv4=1:n_s    %optional: use parallel computing to speed up simulation
for lv4=1:n_s
  m_FCu(:,lv4) = hyb(:,lv4).*(Pmax_el(lv4)*1e6./p.P_sp_FC);
  for lv5=1:n_hyb
    [~,m_Tu(lv5, lv4),~,~] = Tank_submodel(p.TankDesign,...
    p.TankElongation, p.WallMaterial, p.InsulationMaterial,p.InsulationHeatflowshare_PMin,...
    mfH2_Pmin(lv4)/1e3, m_H2u(lv5,lv4)/1e3,...
    p.HydrogenDesignPressure, p.HydrogenStartTemperature, p.WallMinTempereature,...
    p.Additional_Insulation, LH2_density, GH2_density);  
  end;
end;
m_Tu=m_Tu*1e3; % unit conversion kg -> g

%% calculate total mass and determine optimum
m_tot = m_H2u+m_Batu+m_FCu+m_Tu;

for lv4=1:n_s
  [m_opt(lv4),i_opt(lv4)] = min(m_tot(:,lv4));
  hyb_opt(lv4) = hyb(i_opt(lv4));
end

save(strcat('181121_',str_id,'_hybridization_Cholesky.mat'));

figure(8);

subplot(2,1,1);
histogram(m_opt,'Normalization','probability');
xlabel('total system mass /g')
ylabel('rel. #')
title('energy density battery = 1 kWh/kg, power density of FC = 1.6kW/kg');
subplot(2,1,2);
histogram(hyb_opt,'Normalization','probability');
xlabel('degree of hybridization (P_{fc}/P_{fc+bat})')
ylabel('rel. #')


figure(81);
plot(hyb_opt,'x');

sample=1;

figure(82);hold on;
plot(Pv_cH2,m_tot(:,1));
plot(Pv_cH2,m_Batu(:,1));
plot(Pv_cH2,m_FCu(:,1));
plot(Pv_cH2,m_H2u(:,1));
plot(Pv_cH2,m_Tu(:,1));


figure(83);hold on;
plot(Pv_el,m_tot(:,sample));
plot(Pv_el,m_Batu(:,sample));
plot(Pv_el,m_FCu(:,sample));
plot(Pv_el,m_H2u(:,sample));
plot(Pv_el,m_Tu(:,sample));
