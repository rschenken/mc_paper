%% Fuel Cell Model
% 
% Analytical Fuel Cell Model after Kulikovsky 2014 JES 161(3) F263-70

clear all;
close all;clear all; clc; 

%% Parameters

p = polcurve_parameters;

%% inputs

j0_ms = 2.053;  % [A/cm^2] current density

j0 = [0, logspace(-10,-2,20), 0.01:0.001:j0_ms];   % [A/cm^2] current density
%   j_lim = (4*p.F*p.D_b*p.c_h)/p.l_b;
%   j0 = 0:0.01:j_lim;


%% Polarization curve

[V_cell,eta0,P, eta_act,eta_tCCL, eta_tGDL, iR, beta] = polcurve(j0,p);


%% efficiency

eta = V_cell/1.253; % production of water vapor
% eta_th = V_cell/1.481; % production of liquid water

P_nd=P./max(P);
eta_nd=eta./max(eta);


%% Figures

figure(1);hold on;
plot(j0,V_cell);
xlabel('current density / (A/cm^3)');
ylabel('cell voltage / (V)');




figure(2);hold on;
plot(j0,eta0);
xlabel('current density / (A/cm^3)');
ylabel('overpotential / (V)');

figure(21);hold on;
plot(j0,eta_act);
xlabel('current density / (A/cm^3)');
ylabel('activation overpotential / (V)');

figure(22);hold on;
plot(j0,eta_tCCL);
xlabel('current density / (A/cm^3)');
ylabel('overpotential O_2 transport in CCL / (V)');

figure(23);hold on;
plot(j0,eta_tGDL);
xlabel('current density / (A/cm^3)');
ylabel('overpotential O_2 transport in GDL / (V)');

figure(24);hold on;
plot(j0,iR);
xlabel('current density / (A/cm^3)');
ylabel('iR drop / (V)');

figure(25);hold on;
plot(j0,beta);
xlabel('current density / (A/cm^3)');
ylabel('\beta');


figure(3);hold on;
plot(j0,P);
xlabel('current density / (A/cm^3)');
ylabel('power density / (W/cm^2)');













