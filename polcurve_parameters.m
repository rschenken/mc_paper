function p = polcurve_parameters
%

%% natural constants
p.F = 96485.3329;  % [As/mol] Faraday constant; Ref: NIST CODATA recommendet values

%% geometry

p.l_t = 0.001;     % [cm] CL thickness
p.l_b = 0.025;     % [cm] GDL thickness

% not used in the model !?
% p.l_m = 0.0025;    % [cm] membrane thickness

%% material 

% catalyst
p.b = 0.03;        % [V] Tafel slope
p.i_st = 0.817e-3; % [A/cm^3] volumetric exchange currend density
p.sigma_t = 0.03;  % [S/cm^2] CCL proton conductivity
p.D = 1.36e-4;     % [cm^2/s] oxygen diffusion coefficient in the CL

% GDL
p.D_b = 0.0259;    % [cm^2/s] oxygen diffusion coefficient in the GDL

%% cell
p.V_oc = 1.145;    % [V] open circuit voltage
p.R_o = 0.126;     % [Ohm cm^2] cell ohmic resistivity

%% operation
p.c_h = 7.36e-6;% [mol/cm^3] oxygen concentration

%% others
% the big question is: what's the value for c_ref???
% own: O2 concentration under standard conditions of air: 101326 Pa, 298K, 21% O2
% -> this seems to reproduce Fig. 5, curve 1 in the paper
p.c_ref = 8.58335e-6;% [mol/cm^3] reference oxygen concentration

% pure O2 under standard conditions:
% p.c_ref = 4.0874e-05;% [mol/cm^3] reference oxygen concentration

%% Data from Klingele et al.
% p.V_oc = 0.978489175495164;
% p.i_st = 0.042198681176838504;
% p.sigma_t = 0.07285302593659942;
% p.D = 0.00019398756046993778;
% p.D_b = 0.03848596206683334;
% p.R_o = 0;
% p.c_h = p.c_ref;

%% Data from Breitwieser et al.
% p.i_st = 0.01221465483410085;
% p.sigma_t = 0.005014409221902008;
% p.D = 0.0005316516931582585;
% p.D_b = 0.03854105682770301;
% p.R_o = 0;

