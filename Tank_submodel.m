function [tank_volume, tank_mass, mass_energy_density, volume_energy_density] = ...
    Tank_submodel(tank_design, tank_elongation,...
    wall_material, insulation_material, heatflowshare_PMin, hydrogen_massflow_PMin,...
    hydrogen_mass, hydrogen_designpressure, hydrogen_temperature,...
    wall_min_temperature, additional_insulation, LH2_density, GH2_density)
% This function calculates different characteristics of a tank system.

% Unities of the in- and output parameters:

% ->input parameters: 
% 	tank_design:         sphere (2) or cylinder (1) 
%   tank_elongation:     ratio "lambda" of the cylindrical length to the entire length of the tank 
%   wall_material:       number of the used material of the stress bearing component, numbers corresponding to table A.F. in the appendix and wallmaterial_parameters.m
%   insulation_material: number of the used insulation material, number corresponding to table A.G. in the appendix and insulationmaterial_parameters.m
%   heatflowshare_PMin:  share of the entire required vaporization heat that is provided by heat transport through the walls at minimum power
%   hydrogen_massflow_Pmin: minimum required hydrogen massflow during the flight in kg/s
%   hydrogen_mass:       entire hydrogen mass that has to be stored in kg
%   hydrogen_designpressure:   design pressure in the tank at the beginning in bar 
%   Hydrogen_temperature: temperature of the charged hydrogen in K
%   wall_min_temperature: expected minimum temperature of the wall, for example temperature of the stored LH2 or the surrounding, as this leads to different wall material characteristics, in K
%   additional_insulation:outer insulation with a fixed thickness
%   LH2_density:          density of the liquid hydrogen
%   GH2_density:          density of the gaseous hydrogen

% ->output parameters:
% 	tank_volume:           overall-volume of the finished tank             in m^3
%   tank_mass:             mass of the entire tank including H2            in kg
%   mass_energy_density:   mass-specific energy density                    in MJ/kg
%   volume_energy_density: volume-specific energy density                  in MJ/l

hydrogen_designpressure = hydrogen_designpressure * 100000; % converting the pressure in N/m^2
if (tank_elongation == 0), tank_design = 2; end % simplification of the calculations

% Providing the wall material characteristics:
wm = Wallmaterial_parameters(wall_material, wall_min_temperature);
Dimensions_exceed_limits = false;

% Solving the start volume of LH2, using the density of LH2, and adding further 25% of space above the LH2:
hydrogen_volume = hydrogen_mass/LH2_density * 1.042; % in m^3

switch tank_design
    case 1 % Cylindrical design:
        % Transfer the volume into the radius r of a sphere that contains the hydrogen only:
        r_inside = ((hydrogen_volume)/(4/3 * pi + 2 * pi /(1/tank_elongation - 1))).^(1/3); % m
        % Solving the thickness of the wall using the AD2000:
        [wall_thickness_s, wall_thickness_c] = AD2000(tank_design, tank_elongation, hydrogen_designpressure * wm.Fp, 2*r_inside, wm); % m
        % Steels have a maximal allowed diameter which has to checked:
        if wall_material > 50 && wall_material < 101 && (wm.smax < wall_thickness_s || wm.dmax < 2 * r_inside)
            Dimensions_exceed_limits = true;
        else
            % Calculating the length of the cylindrical part:
            l_c = 2 * r_inside / (1/tank_elongation - 1);
            % Calculating the volume of the wall:
            wall_volume = (4/3)*pi * ((r_inside+wall_thickness_s).^3 - r_inside.^3) + pi * ((r_inside + wall_thickness_c)^2 - r_inside^2) * l_c; % in m^3
            % Calculate the insulation mass:   
            [insulation_mass, insulation_thickness_s, insulation_thickness_c] = Insulation_calculator(tank_design, insulation_material, wm, r_inside, l_c, wall_thickness_s, wall_thickness_c, hydrogen_massflow_PMin, 1, additional_insulation, hydrogen_temperature, 288.15, LH2_density, GH2_density); % in kg, m
            % Calculating the volume of the tank:
            tank_volume = (4/3) * pi * (r_inside + wall_thickness_s + insulation_thickness_s + additional_insulation).^3 + pi * (r_inside + wall_thickness_c + insulation_thickness_c + additional_insulation).^2 * l_c; % in m^3
        end
         
    case 2 % Spherical design:
        % Transfer the volume into the radius r of a sphere that contains the hydrogen only:
        r_inside = ((3/4)*(hydrogen_volume/pi)).^(1/3); % in m
        % Solving the thickness of the wall using the AD2000:
        [wall_thickness_s, ~] = AD2000(tank_design, tank_elongation, hydrogen_designpressure * wm.Fp, 2*r_inside, wm); % in m
        % Steels have a maximal allowed diameter which has to checked:
        if wall_material > 50 && wall_material < 101 && (wm.smax < wall_thickness_s || wm.dmax < 2 * r_inside)
            Dimensions_exceed_limits = true;
        else
            % Calculating the volume of the wall:
            wall_volume = (4/3)*pi * ((r_inside + wall_thickness_s).^3 - r_inside.^3); % m^3
            % Calculate the insulation:      
            [insulation_mass, insulation_thickness_s, ~] = Insulation_calculator(tank_design, insulation_material, wm, r_inside, 0, wall_thickness_s, 0, hydrogen_massflow_PMin, heatflowshare_PMin, additional_insulation, hydrogen_temperature, 288.15, LH2_density, GH2_density); % in kg, m
            % Calculation of the volume of the entire tank:
            tank_volume = (4/3) * pi * (r_inside+wall_thickness_s + insulation_thickness_s + additional_insulation).^3; % m^3
        end
end       

if Dimensions_exceed_limits == false
    % Calculation the wall mass out of the volume and density:
    wall_mass = wall_volume * wm.d; % kg
    % Calculation of the entire tanks mass out of the wall material, stored hydrogen and insulation:
    tank_mass =  wall_mass + hydrogen_mass + insulation_mass; % kg
    % Calculation of the mass - specific energy density:
    mass_energy_density = (hydrogen_mass * 120)/tank_mass; % MJ/kg
    % Calculation of the volume - specific energy density:
    volume_energy_density = (hydrogen_mass * 120)/(1000*tank_volume); % MJ/l
else
    tank_mass = 0;
    tank_volume = 0;
    mass_energy_density = 0;
    volume_energy_density = 0;
end