% The file performs a covariance decomposition and saves the results
%   author: Ulrich Roemer
%   description: 
%       -input are random histograms stored in raw_data_hist_A320/B772.mat
%       -x represents power consumption
%       -for eigendecomposition, the true distribution of the new variables
%       is approximated with a Kernel density estimate; Cholesky assumes a
%       Gaussian distribution for the new variables

% ---------- Custom settings 
% choose data (hist: data containing t-P histograms)
%str_id = 'hist_A320';     
str_id = 'hist_B772';   

% flag to choose between Cholesky (chol=1) and PCA (chol=0)
chol_flg = 1;
%----------

int = 0;
% load data
load(strcat('raw_data_',str_id,'.mat'))

x_max = length(X(1,:));    % reconstruct max x-value 
dx = 1:x_max;    % points on x-axis

% setup covariance depending on flags
if strcmp(str_id, 'hist_B772')    % coarse grid sampling (use to homogenize length for all airplane types)
    x_sub = 1:1:x_max;
    M = length(x_sub);
    B = zeros(x_max,M);
    for i = 1:M
        y_sub = zeros(size(x_sub));
        y_sub(i) = 1;
        tmp = interp1(x_sub,y_sub,dx);
        B(:,i) = tmp;
    end
    cov_X_full = cov(X);   
    cov_X = cov_X_full(x_sub,x_sub);
    X = X(:,x_sub);
else    
    M = size(X,1);
    cov_X = cov(X);       
end

% compute mean value
X_mean = mean(X,1); 
X = X - repmat(X_mean,[size(X,1),1]);

if chol_flg ==1
    % Cholesky decomposition
    L = chol(cov_X + 1e-8*eye(M),'lower');
    L(isnan(L)) = 0;
else
    % Principal Component Analysis
    M_KLE = 75;
    [V,E] = eigs(cov_X,M_KLE);    
    L = zeros(M,M_KLE);    
    Z = zeros(M_KLE,size(X,1));
    for j = 1:M_KLE    
        L(:,j) = V(:,j)*E(j,j)^(1/2);
        for i = 1:size(X,1)
            Z(j,i) = 1/sqrt(E(j,j))*X(i,:)*V(:,j);                         
        end
    end
    L(isnan(L)) = 0;
    %Z = X*V*diag(diag(E.^(-1/2))); 
    dens = cell(1,M_KLE);
    for i = 1:M_KLE
        dens{i} = fitdist(Z(i,:)','Kernel','Kernel','epanechnikov');   
    end
end

% save decomposition data
decomp_dat = struct;
decomp_dat.mean_value = X_mean;
decomp_dat.L = L;
if ~chol_flg
    decomp_dat.dens = dens;
end

save(strcat('decomp_',str_id,'.mat'),'decomp_dat')






