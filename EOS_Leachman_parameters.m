function p = EOS_Leachman_parameters(hydrogen_specification)
% This function returns a data set p of parameters that are required for
% the function "EOS_Leachman_parameters".
%% hydrogen_specification: either 'normal', 'para' or 'ortho' hydrogen

switch hydrogen_specification
 
%% normal hydrogen:
case 'normal'
    
% Hydrogen's constants:
p.Tc = 33.145;   %  K
p.rhoc = 31.263; %  kg/m^3

% Constants for alpha_0:
p.a1 = - 1.4579856475;
p.a2 = 1.888076782;
p.a3 = 1.616;
p.a4 = -0.4117;
p.a5 = -0.792;
p.a6 = 0.758;
p.a7 = 1.217;
p.a = [p.a1, p.a2, p.a3, p.a4, p.a5, p.a6, p.a7];

p.b1 = 0;
p.b2 = 0;
p.b3 = -16.0205159149;
p.b4 = -22.6580178006;
p.b5 = -60.0090511389;
p.b6 = -74.9434393817;
p.b7 = -206.9392065168;
p.b = [p.b1, p.b2, p.b3, p.b4, p.b5, p.b6, p.b7];

% Constanst for alpha_r:
p.N1 = -6.93643;
p.N2 = 0.01;
p.N3 = 2.1101;
p.N4 = 4.52059;
p.N5 = 0.732564;
p.N6 = -1.34086;
p.N7 = 0.130985;
p.N8 = -0.777414;
p.N9 = 0.351944;
p.N10 = -0.0211716;
p.N11 = 0.0226312;
p.N12 = 0.032187;
p.N13 = -0.0231752;
p.N14 = 0.0557346;
p.N = [p.N1, p.N2, p.N3, p.N4, p.N5, p.N6, p.N7, p.N8, p.N9, p.N10, p.N11, p.N12, p.N13, p.N14];

p.t1 = 0.6844;
p.t2 = 1;
p.t3 = 0.989;
p.t4 = 0.489;
p.t5 = 0.803;
p.t6 = 1.1444;
p.t7 = 1.409;
p.t8 = 1.754;
p.t9 = 1.311;
p.t10 = 4.187;
p.t11 = 5.646;
p.t12 = 0.791;
p.t13= 7.249;
p.t14 = 2.986;
p.t = [p.t1, p.t2, p.t3, p.t4, p.t5, p.t6, p.t7, p.t8, p.t9, p.t10, p.t11, p.t12, p.t13, p.t14];

p.d1 = 1;
p.d2 = 4;
p.d3 = 1;
p.d4 = 1;
p.d5 = 2;
p.d6 = 2;
p.d7 = 3;
p.d8 = 1;
p.d9 = 3;
p.d10 = 2;
p.d11 = 1;
p.d12 = 3;
p.d13 = 1;
p.d14 = 1;
p.d = [p.d1, p.d2, p.d3, p.d4, p.d5, p.d6, p.d7, p.d8, p.d9, p.d10, p.d11, p.d12, p.d13, p.d14];

p.p1 = 0;
p.p2 = 0;
p.p3 = 0;
p.p4 = 0;
p.p5 = 0;
p.p6 = 0;
p.p7 = 0;
p.p8 = 1;
p.p9 = 1;
p.p = [p.p1, p.p2, p.p3, p.p4, p.p5, p.p6, p.p7, p.p8, p.p9];

p.phi10 = -1.685;
p.phi11 = -0.489;
p.phi12 = -0.103;
p.phi13 = -2.506;
p.phi14 = -1.607;
p.phi = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.phi10, p.phi11, p.phi12, p.phi13, p.phi14]; 

p.betta10 = -0.171;
p.betta11 = -0.2245;
p.betta12 = -0.1304;
p.betta13 = -0.2785;
p.betta14 = -0.3967;
p.betta = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.betta10, p.betta11, p.betta12, p.betta13, p.betta14];

p.gamma10 = 0.7164;
p.gamma11 = 1.3444;
p.gamma12 = 1.4517;
p.gamma13 = 0.7204;
p.gamma14 = 1.5445;
p.gamma = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.gamma10, p.gamma11, p.gamma12, p.gamma13, p.gamma14];

p.D10 = 1.506;
p.D11 = 0.156;
p.D12 = 1.736;
p.D13 = 0.67;
p.D14 = 1.662;
p.D = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.D10, p.D11, p.D12, p.D13, p.D14];

%% Parahydrogen      
case 'para'
% Hydrogen's constants:
p.Tc = 32.938;   %  K
p.rhoc = 31.323; %  kg/m^3

% Constants for alpha_0:
p.a1 = - 1.4485891134;
p.a2 = 1.884521239;
p.a3 = 4.30256;
p.a4 = 13.0289;
p.a5 = -47.7365;
p.a6 = 50.0013;
p.a7 = -18.6261;
p.a8 = 0.993973;
p.a9 = 0.536078;
p.a = [p.a1, p.a2, p.a3, p.a4, p.a5, p.a6, p.a7, p.a8, p.a9];

p.b1 = 0;
p.b2 = 0;
p.b3 = -15.1496751472;
p.b4 = -25.0925982148;
p.b5 = -29.4735563787;
p.b6 = -35.4059141417;
p.b7 = -40.724998482;
p.b8 = -163.7925799988;
p.b9 = -309.2173173842;
p.b = [p.b1, p.b2, p.b3, p.b4, p.b5, p.b6, p.b7, p.b8, p.b9];

% Constanst for alpha_r:
p.N1 = -7.33375;
p.N2 = 0.01;
p.N3 = 2.60375;
p.N4 = 4.66279;
p.N5 = 0.682390;
p.N6 = -1.47078;
p.N7 = 0.135801;
p.N8 = -1.05327;
p.N9 = 0.328239;
p.N10 = -0.0577833;
p.N11 = 0.0449743;
p.N12 = 0.0703464;
p.N13 = -0.0401766;
p.N14 = 0.119510;
p.N = [p.N1, p.N2, p.N3, p.N4, p.N5, p.N6, p.N7, p.N8, p.N9, p.N10, p.N11, p.N12, p.N13, p.N14];

p.t1 = 0.6855;
p.t2 = 1;
p.t3 = 1;
p.t4 = 0.489;
p.t5 = 0.774;
p.t6 = 1.133;
p.t7 = 1.386;
p.t8 = 1.619;
p.t9 = 1.162;
p.t10 = 3.96;
p.t11 = 5.276;
p.t12 = 0.99;
p.t13= 6.791;
p.t14 = 3.19;
p.t = [p.t1, p.t2, p.t3, p.t4, p.t5, p.t6, p.t7, p.t8, p.t9, p.t10, p.t11, p.t12, p.t13, p.t14];

p.d1 = 1;
p.d2 = 4;
p.d3 = 1;
p.d4 = 1;
p.d5 = 2;
p.d6 = 2;
p.d7 = 3;
p.d8 = 1;
p.d9 = 3;
p.d10 = 2;
p.d11 = 1;
p.d12 = 3;
p.d13 = 1;
p.d14 = 1;
p.d = [p.d1, p.d2, p.d3, p.d4, p.d5, p.d6, p.d7, p.d8, p.d9, p.d10, p.d11, p.d12, p.d13, p.d14];

p.p1 = 0;
p.p2 = 0;
p.p3 = 0;
p.p4 = 0;
p.p5 = 0;
p.p6 = 0;
p.p7 = 0;
p.p8 = 1;
p.p9 = 1;
p.p = [p.p1, p.p2, p.p3, p.p4, p.p5, p.p6, p.p7, p.p8, p.p9];

p.phi10 = -1.7439;
p.phi11 = -0.5516;
p.phi12 = -0.0634;
p.phi13 = -2.1341;
p.phi14 = -1.777;
p.phi = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.phi10, p.phi11, p.phi12, p.phi13, p.phi14]; 

p.betta10 = -0.194;
p.betta11 = -0.2019;
p.betta12 = -0.0301;
p.betta13 = -0.2383;
p.betta14 = -0.3253;
p.betta = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.betta10, p.betta11, p.betta12, p.betta13, p.betta14];

p.gamma10 = 0.8048;
p.gamma11 = 1.5248;
p.gamma12 = 0.6648;
p.gamma13 = 0.6832;
p.gamma14 = 1.493;
p.gamma = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.gamma10, p.gamma11, p.gamma12, p.gamma13, p.gamma14];

p.D10 = 1.5487;
p.D11 = 0.1785;
p.D12 = 1.28;
p.D13 = 0.6319;
p.D14 = 1.7104;
p.D = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.D10, p.D11, p.D12, p.D13, p.D14];
    
%% Orthohydrogen        
case 'ortho'
    
% Hydrogen's constants:
p.Tc = 33.22;   %  K
p.rhoc = 31.135; %  kg/m^3

% Constants for alpha_0:
p.a1 = -1.4675442336;
p.a2 = 1.8845068862;
p.a3 = 2.54151;
p.a4 = -2.3661;
p.a5 = 1.00365;
p.a6 = 1.22447;
p.a = [p.a1, p.a2, p.a3, p.a4, p.a5, p.a6];

p.b1 = 0;
p.b2 = 0;
p.b3 = -25.7676098736;
p.b4 = -43.4677904877;
p.b5 = -66.0445514750;
p.b6 = -209.753160746;
p.b = [p.b1, p.b2, p.b3, p.b4, p.b5, p.b6];

% Constanst for alpha_r:
p.N1 = -6.83148;
p.N2 = 0.01;
p.N3 = 2.11505;
p.N4 = 4.38353;
p.N5 = 0.211292;
p.N6 = -1.00939;
p.N7 = 0.142086;
p.N8 = -0.87696;
p.N9 = 0.804927;
p.N10 = -0.710775;
p.N11 = 0.0639688;
p.N12 = 0.0710858;
p.N13 = -0.087654;
p.N14 = 0.647088;
p.N = [p.N1, p.N2, p.N3, p.N4, p.N5, p.N6, p.N7, p.N8, p.N9, p.N10, p.N11, p.N12, p.N13, p.N14];

p.t1 = 0.7333;
p.t2 = 1;
p.t3 = 1.1372;
p.t4 = 0.5136;
p.t5 = 0.5638;
p.t6 = 11.6248;
p.t7 = 1.829;
p.t8 = 2.404;
p.t9 = 2.105;
p.t10 = 4.1;
p.t11 = 7.658;
p.t12 = 1.259;
p.t13= 7.589;
p.t14 = 3.946;
p.t = [p.t1, p.t2, p.t3, p.t4, p.t5, p.t6, p.t7, p.t8, p.t9, p.t10, p.t11, p.t12, p.t13, p.t14];

p.d1 = 1;
p.d2 = 4;
p.d3 = 1;
p.d4 = 1;
p.d5 = 2;
p.d6 = 2;
p.d7 = 3;
p.d8 = 1;
p.d9 = 3;
p.d10 = 2;
p.d11 = 1;
p.d12 = 3;
p.d13 = 1;
p.d14 = 1;
p.d = [p.d1, p.d2, p.d3, p.d4, p.d5, p.d6, p.d7, p.d8, p.d9, p.d10, p.d11, p.d12, p.d13, p.d14];

p.p1 = 0;
p.p2 = 0;
p.p3 = 0;
p.p4 = 0;
p.p5 = 0;
p.p6 = 0;
p.p7 = 0;
p.p8 = 1;
p.p9 = 1;
p.p = [p.p1, p.p2, p.p3, p.p4, p.p5, p.p6, p.p7, p.p8, p.p9];

p.phi10 = -1.169;
p.phi11 = -0.894;
p.phi12 = -0.04;
p.phi13 = -2.072;
p.phi14 = -1.306;
p.phi = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.phi10, p.phi11, p.phi12, p.phi13, p.phi14]; 

p.betta10 = -0.4555;
p.betta11 = -0.4046;
p.betta12 = -0.0869;
p.betta13 = -0.4415;
p.betta14 = -0.5743;
p.betta = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.betta10, p.betta11, p.betta12, p.betta13, p.betta14];

p.gamma10 = 1.5444;
p.gamma11 = 0.6627;
p.gamma12 = 0.763;
p.gamma13 = 0.6587;
p.gamma14 = 1.4327;
p.gamma = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.gamma10, p.gamma11, p.gamma12, p.gamma13, p.gamma14];

p.D10 = 0.6366;
p.D11 = 0.3876;
p.D12 = 0.9437;
p.D13 = 0.3976;
p.D14 = 0.9626;
p.D = [0, 0, 0, 0, 0, 0, 0, 0, 0, p.D10, p.D11, p.D12, p.D13, p.D14];
        
end

end 