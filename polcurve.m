function [V_cell,varargout]=polcurve(j0,p)
%POLCURVE  Analytical Fuel Cell Model after Kulikovsky 2014 JES 161(3) F263-70
%
%   [V_cell] = polcurve(j_0,p)  returns the cell voltage V_cell for a given vector of cell current densities j_0   
%
%   [V_cell, eta_0] = polcurve(j_0,p)  additionally returns the cell overpotential eta_0
%
%   [V_cell, eta_0, P] = polcurve(j_0,p)  additionally returns the cell power density
%

%% Help Equations

j_sigma = sqrt(2*p.i_st*p.sigma_t*p.b);
j_st = p.sigma_t*p.b/p.l_t;
beta = sqrt(2*j0/j_st)./(1+sqrt(1.12*j0/j_st).*exp(sqrt(2*j0/j_st))) + pi*j0/j_st./(2+j0/j_st);
j_lim = (4*p.F*p.D_b*p.c_h)/p.l_b;


%% Overpotential

eta0 = p.b*asinh((j0/j_sigma).^2./(2*(p.c_h/p.c_ref)*(1-exp(-j0/(2*j_st)))))...
     + p.sigma_t.*(p.b^2)./(4.*p.F.*p.D.*p.c_h).* (j0./j_st - log(1+j0.^2./(j_st.^2.*beta.^2)))./(1- j0./(j_lim.*(p.c_h./p.c_ref)))...
     - p.b*log(1- j0/(j_lim*(p.c_h/p.c_ref))); % [V]
 
if nargout>2
    eta_actpt = p.b*asinh((j0/j_sigma).^2./(2*(p.c_h/p.c_ref)*(1-exp(-j0/(2*j_st)))));
    eta_tCCL = p.sigma_t*p.b^2/(4*p.F*p.D*p.c_h)* (j0/j_st - log(1+j0.^2./(j_st^2*beta.^2)))./(1- j0/(j_lim*(p.c_h/p.c_ref)));
    eta_tGDL = - p.b*log(1- j0/(j_lim*(p.c_h/p.c_ref)));
    iR = p.R_o*j0;
    varargout{3} = eta_actpt;
    varargout{4} = eta_tCCL;
    varargout{5} = eta_tGDL;
    varargout{6} = iR;
    varargout{7} = beta;
end


%% Cell Voltage 

V_cell = p.V_oc - eta0 - p.R_o*j0;  % [V]


%% Power Density

P = V_cell .* j0;


%% Outputs
varargout{1} = eta0;
varargout{2} = P;


end

