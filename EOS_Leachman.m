function [hydrogen_volume, hydrogen_pressure, rho_H2] = EOS_Leachman(searched_characteristic, hydrogen_temperature, hydrogen_mass, hydrogen_pressure, hydrogen_density, hydrogen_specification)
% This function calculates either the hydrogen`s volume or pressure.

% Unitys of the In- and Output parameters:
% searched_characteristic: (1): volume; (2): pressure
% hydrogen_temperature    K
% hydrogen_mass           kg
% hydrogen_pressure       N/m^2 = 1*10^-5 bar
% hydrogen_density        kg/m^3
% hydrogen_specification: either 'para', 'ortho' or 'normal'
% hydrogen_volume         m^3

T = hydrogen_temperature;
syms x;
% constants for the calculation:
p = EOS_Leachman_parameters(hydrogen_specification);

% forming the sums of alpha_0:
switch hydrogen_specification
case 'normal' 
sum11 = 0;
for i=3:1:7
    sum11 = sum11 + (p.a(1,i) * log(1 - exp(p.b(1,i) * (p.Tc/T))));
end

case 'para'
sum11 = 0;
for i=3:1:9
    sum11 = sum11 + (p.a(1,i) * log(1 - exp(p.b(1,i) * (p.Tc/T))));
end

case 'ortho'
sum11 = 0;
for i=3:1:6
    sum11 = sum11 + (p.a(1,i) * log(1 - exp(p.b(1,i) * (p.Tc/T))));
end      
end

% forming alpha_0:
alpha_0 = log(x/p.rhoc) + 1.5 * log(p.Tc/T) + p.a1 + p.a2 * (p.Tc/T) + sum11;


% forming the first sum of alpha_r:
sum21 = 0;
for i = 1:1:7
    sum21 = sum21 + p.N(1,i) * (x/p.rhoc)^p.d(1,i) * (p.Tc/T)^p.t(1,i);
end

% forming the second sum of alpha_r:
sum22 = 0;
for i = 8:1:9 
    sum22 = sum22 + p.N(1,i) * (x/p.rhoc)^p.d(1,i) * (p.Tc/T)^p.t(1,i) * exp(-((x/p.rhoc)^p.p(1,i)));
end

% forming the third sum of alpha_r:
sum23 = 0;
for i = 10:1:14
    sum23 = sum23 + p.N(1,i) * (x/p.rhoc)^p.d(1,i) * (p.Tc/T)^p.t(1,i) * exp(p.phi(1,i) * (((x/p.rhoc) - p.D(1,i))^2) + p.betta(1,i) * (((p.Tc/T) - p.gamma(1,i))^2));
end

% forming alpha_r:
alpha_r = sum21 + sum22 + sum23;

% forming alpha_entire:
alpha_entire = alpha_0 + alpha_r;

% differenciation of alpha_entire with respect to the density "x", multiply with RTx^2 and substract the pressure:
c = diff(alpha_entire,x) * T * 4124.2 * x^2 - hydrogen_pressure;
d = matlabFunction(c);

if searched_characteristic == 1
    % finding the zeroes:
    x0 = [0.1 100];
    rho_H2 = fzero(d, x0);

    % calculation of the volume using the mass:
    hydrogen_volume = hydrogen_mass/rho_H2;
else
    x = hydrogen_density;
    hydrogen_pressure = d(x);
    hydrogen_volume = 0;
    rho_H2 = hydrogen_density;
end