function [insulation_mass, insulation_thickness_s, insulation_thickness_c]...
    = Insulation_calculator(tank_design, insulation_material, wall_material,...
    r_inside, l_c, wall_thickness_s, wall_thickness_c, hydrogen_massflow,...
    insulation_heatflowshare, additional_insulation, hydrogen_temperature,...
    surrounding_temperature, LH2_density, GH2_density)
% This function calculates the insulation's mass & thickness, the heater's
% characteristics and the mass flow of vaporized hydrogen before take-off.

% Unities of the input paramters:
%   tank_design: spherical (2) or cylindrical (1) shape
%   insulation_material: number of the used insulation material, number corresponding to table A.G. in the appendix and insulationmaterial_parameters.m
%   wall_material: data set of wall material characteristics, called by the main function and provided by "Wallmaterial_parameters.m"
%   r_inside: inner radius of the wall                                     in m
%   l_c: length of the cylindrical part of a cylindrical tank              in m
%   wall_thickness_s: wall's thickness of the spherical part of the wall   in m
%   wall_thickness_c: wall's thickness of the cylindrical part of the wall in m
%   hydrogen_massflow: required mass flow of hydrogen at the seign point   in kg/s
%   insulation_heatflowshare: share of the entire required vaporization heat that is provided by heat transport through the walls
%   Additional_Insulation:   outer isolation with a fixed thickness        in m
%   Hydrogen_temperature: temperature of the charged hydrogen              in K
%   surrounding_temperature: temperature of the surrounding air            in K
%   LH2_density:          density of the liquid hydrogen                   in kg/m^3
%   GH2_density:          density of the gaseous hydrogen in               kg/m^3

% Unities of the output parameters:
%   insulation_mass: entire mass of the insulation                         in kg
%   insulation_thickness_s: insulation's thickness of spherical sections   in m
%   insulation_thickness_c: insulation's thickness of cylindrical sections in m

syms insulation_thickness_s;
syms insulation_thickness_c;

% Providing the insulation material parameters:
im = Insulationmaterial_parameters(insulation_material); % material of the actual insulation layer of closed cell foam
oc = Insulationmaterial_parameters(11); % material of the second layer out of open cell foam

% Calculating the required parameters:
A_in_wall_s = 4 * pi * (r_inside)^2; % m^2
A_out_wall_s = 4 * pi * (r_inside + wall_thickness_s)^2;
A_m_wall_s = sqrt(A_out_wall_s*A_in_wall_s);
A_in_insulation_s = A_out_wall_s;
A_out_insulation_s = 4 * pi * (r_inside + wall_thickness_s + insulation_thickness_s)^2;
A_m_insulation_s = sqrt(A_in_insulation_s * A_out_insulation_s);
A_m_opencell_s = 4 * pi * (r_inside + wall_thickness_s + insulation_thickness_s + 0.5 * additional_insulation)^2;
alpha_in_s = 17 * 0.186/(2*r_inside); % in W/m^2K
alpha_out_s = 2 * 24.38/(2*(r_inside + wall_thickness_s + insulation_thickness_s + additional_insulation));

% Further parameters if there is a cylindrical part:
A_in_wall_c = 0;
if tank_design == 1
    A_in_wall_c = 2 * pi * r_inside * l_c;
    A_out_wall_c = 2 * pi * (r_inside + wall_thickness_c) * l_c;
    A_m_wall_c = sqrt(A_in_wall_c * A_out_wall_c);
    A_in_insulation_c = 2 * pi * (r_inside + wall_thickness_c) * l_c;
    A_out_insulation_c = 2 * pi * (r_inside + wall_thickness_c + insulation_thickness_c) * l_c;
    A_m_insulation_c = sqrt(A_in_insulation_c * A_out_insulation_c);
    A_m_opencell_c = 2 * pi * (r_inside + wall_thickness_c + insulation_thickness_c + 0.5 * additional_insulation) * l_c;
    alpha_in_c = 17 * 0.186/((pi/2)*2*(r_inside));
    alpha_out_c = 0.3 * 24.38/((pi/2)*(2*(r_inside + wall_thickness_c + insulation_thickness_c + additional_insulation)));
end

% Calculating the required heat flow at maximum power:
Q = hydrogen_massflow * (451900 *(1  +(GH2_density/(GH2_density + LH2_density)))) * 0.5  * 2/3 * insulation_heatflowshare; % in W

ratio = A_in_wall_c/(A_in_wall_s + A_in_wall_c);
if ratio >= 1
   Q_c = (ratio/(ratio+1)) * Q; 
   Q_s = Q - Q_c;
else
   Q_c = ratio * Q;
   Q_s = Q - Q_c;
end

% Calculating the thermal transition resistance and the insulation_thicknesses_s required during take off:
kA_s = 1/((1/(alpha_in_s * A_in_wall_s)) + (wall_thickness_s/(wall_material.ht * A_m_wall_s)) + (insulation_thickness_s/(im.ht * A_m_insulation_s))+ (additional_insulation/(oc.ht * A_m_opencell_s)) + (1/(alpha_out_s * A_out_insulation_s)));
c = (Q_s/(surrounding_temperature-hydrogen_temperature)) - kA_s;
d = matlabFunction(c);
x0 = [0.0001 2];
try
    insulation_thickness_s = fzero(d, x0);
    if insulation_thickness_s < 0
        insulation_thickness_s = 0;
    end
catch
    insulation_thickness_s = 0;
end

% If the shape is cylindrical, it's insulation_thickness_c is calculated:
if tank_design == 1
    kA_c = 1/((1/(alpha_in_c * A_in_wall_c)) + (wall_thickness_c/(wall_material.ht * A_m_wall_c)) + (insulation_thickness_c/(im.ht * A_m_insulation_c))+ (additional_insulation/(oc.ht * A_m_opencell_c))+ (1/(alpha_out_c * A_out_insulation_c)));
    c = (Q_c/(surrounding_temperature-hydrogen_temperature)) - kA_c;
    d = matlabFunction(c);
    x0 = [0.0001 2];
    try
        insulation_thickness_c = fzero(d, x0);
        if insulation_thickness_c < 0
            insulation_thickness_c = 0;
        end
    catch
        insulation_thickness_c = 0;
    end
else
    insulation_thickness_c = 0;
end


% Calculating the insulation volume, mass and final thicknesses (adding the open-cell foam layer):
insulation_volume_s = (4/3)*pi * ((r_inside + wall_thickness_s + insulation_thickness_s).^3 - (r_inside + wall_thickness_s).^3);
insulation_volume_c = 2*pi*l_c* ((r_inside + wall_thickness_c + insulation_thickness_c) - (r_inside + wall_thickness_c)); 
insulation_volume = insulation_volume_s + insulation_volume_c;
opencellfoam_mass = oc.ht * (((4/3)*pi * ((r_inside + wall_thickness_s + insulation_thickness_s + additional_insulation).^3 - (r_inside + wall_thickness_s + insulation_thickness_s).^3)) + 2*pi*l_c* ((r_inside + wall_thickness_c + insulation_thickness_c + additional_insulation) - (r_inside + wall_thickness_c + insulation_thickness_c))); % in kg
insulation_mass = insulation_volume * im.d + opencellfoam_mass; 
end