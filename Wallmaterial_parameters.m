function m = Wallmaterial_parameters(material, wall_min_temperature)
% This function returns a data set of material parameters in dependency of the chosen material. 

% Unities of the In- and Output parameters:
%   material: numbers corresponding to table A.F.
%   wall_min_temperature: expected minimum temperature of the wall during a flight due to LH2 or the surrounding, in K
%   m: represents the kind of used material, as it depends on this number what kind of chapter has to be used in the wallthickness calculation (B1(0) or N1)
%   S: security factor, for all kind of aluminum materials 1,5
%   v: factor to considerate the influece of links                         in N/m^2
%   C1: factor to considerate the undercut of the minimal wall thicknee    in m
%   C2: factor to considerate the wastage                                  in m
%   K: strenght parameter                                                  in N/m^2
%   d: density                                                             in kg/m^3
%   ht: thermal conductivity                                               in W/Km

if material<100 % Aluminum Alloys and steel
    m.m = 1; % represents aluminum and aluminum alloys
    m.S = 1.5; % safety factor S is 1.5 for all times
    m.v = 1.0; % Influence of weldings, only localy increased wall thicknesses so that this factor can be unattendet
    m.c1 = 0; % undercuts of the wall thicknesses are not considered in this thesis
    m.c2 = 0; % for all non iron materials and austenitic steels this addition is always zero
    m.Fp = 1.43; % at least 1,43, further calculation below
    if material<51
        m.thickness_wall_minimum = 0.003; % in m, 3mm for aluminum alloys
    else
        m.thickness_wall_minimum = 0.002; % in m, 2mm for steel
        m.smax = 0.075; % maximum wall thickness of steels
    end
end

if (material>100&&material) % GFK materials:
    m.m = 2; % represents GFK materials
    m.S = 2; % global safety factor
    m.Fp = 1.3; % for all times 1,3
end


switch material
    case 1 % EN AW 1098
        m.K = 15; % reduced tenacity at design temperature of 100C
        m.K0 = 17; % tenacity at 20C
        m.d = 2700; % density of the material, in kg/m^3
        m.ht = 232; % thermal conductivity, in W/mK
    case 2 % EN AW 1080A
        m.K = 20;
        m.K0 = 22;
        m.d = 2700;
        m.ht = 225;
    case 3 % EN AW 1070A
        m.K = 23;
        m.K0 = 25;
        m.d = 2700;
        m.ht = 225;
    case 4 % EN AW 1050A
        m.K = 29;
        m.K0 = 30;
        m.d = 2700;
        m.ht = 210;
    case 5 % EN AW 3003
        m.K = 35;
        m.K0 = 35;
        m.d = 2730;
        m.ht = 160;
    case 6 % EN AW 3103
        m.K = 35;
        m.K0 = 35;
        m.d = 2730;
        m.ht = 160;
    case 7 % EN AW 6060
        m.K = 65;
        m.K0 = 65;
        m.d = 2700;
        m.ht = 200;
    case 8 % EN AW 5754
        m.K = 80;
        m.K0 = 80;
        m.d = 2660;
        m.ht = 140;
    case 9 % EN AW 5049
        m.K = 100;
        m.K0 = 100;
        m.d = 2710;
        m.ht = 140;
    case 10 % EN AW 5083
        m.K = 130;
        m.K0 = 130;
        m.d = 2660;
        m.ht = 110;
    case 11 % EN AW 2219
        m.K = 276;
        m.K0 = 276;
        m.d = 2840;
        m.ht = 130;
    case 51 % X6CrNiNb18-10
        m.K = 229;
        m.K0 = 240;
        m.d = 7900;
        m.ht = 15; 
        m.dmax = 0.45;
        m.S = 1.05;
        if wall_min_temperature < 73.15
            m.Sc = 1.5 * (4/3);
        end
    case 52 % X3CrNiMo18-12-3
        m.K = 220;
        m.K0 = 210;
        m.d = 8000;
        m.ht = 15;
        m.dmax = 0.45;
        m.S = 1.05;
        if wall_min_temperature < 73.15
            m.Sc = 1.5 * (4/3);
        end
    case 53 % X6CrNiTi18-10
        m.K = 205;
        m.K0 = 225; 
        m.d = 7900;
        m.ht = 15;
        m.dmax = 0.45;
        m.S = 1.05;
    case 54 % X6CrNiMoTi17-12-2
        m.K = 215;
        m.K0 = 235; 
        m.d = 8000;
        m.ht = 15;
        m.dmax = 0.45;
        m.S = 1.05;
    case 101 % GRP
        m.K_circum = 1800; 
        m.K_long = 1800; 
        m.d = 2450;
        m.ht = 5;
        m.A1 = 2; %
        m.A2 = 1.1; %
        m.A3 = 1.044; % 
        m.A4 = 1.2;
        m.A = m.A1 * m.A2 * m.A3 * m.A4;
        m.thickness_wall_minimum = 0.001;
end
if material<101
    m.K = m.K * 1000000; % MPa in N/m^2
    m.Fp_test = 1.25 * (m.K0/m.K);
    if m.Fp_test>m.Fp
        m.Fp = m.Fp_test;
    end
end
if material>100
    m.K_circum = m.K_circum * 1000000;
    m.K_long = m.K_long * 1000000;
end