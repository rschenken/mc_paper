function [thickness_wall_s, thickness_wall_c] = AD2000(tank_design, tank_elongation, overpressure, innerdiameter, materialparameters)
% This function calculates the thicknesses thickness_wall_s (spherical part) and thickness_wall_c (cylindrical part) of a pressurized wall.

% Unities of the in- and output parameters:

% tank_design: sphere(2) or cylinder(1)
% tank_elongation: elongation "eta" of a cylindrical tank
% overpressure: pressure difference between the in- and outside of the tank        in N/m^2
% innerdiameter: inner diameter of the sphere or cylinder                          in m
% materialparameters: data set of material parameters such as strength or density
% thickness_wall_s/c: calculated thickness of the wall                             in m


switch materialparameters.m
    case 1 % aluminum alloys and steel:
        % Calculations after B1:
        % Calculating the wall thickness of a sphereical section:
        h_s = 2*20 * (materialparameters.K/materialparameters.S) * materialparameters.v + overpressure;
        thickness_wall_s = (materialparameters.c1 + materialparameters.c2 + (innerdiameter * overpressure)/h_s)/(1-(2 * overpressure)/h_s); % in m
        % Calculating the wall thickness of a cylindrical section:
        h_c = 1*20 * (materialparameters.K/materialparameters.S) * materialparameters.v + overpressure;
        thickness_wall_c = (materialparameters.c1 + materialparameters.c2 + (innerdiameter * overpressure)/h_c)/(1-(2 * overpressure)/h_c); % in m
        % Testing the ratio condition of cylindrical sections:
        ratio = (innerdiameter + 2 * thickness_wall_c)/(innerdiameter);
        % Calculating the new thickness after B10 for cylindrical sections if condition (ratio>1.2) is fullfilled: 
        if tank_design==1&&ratio>1.2&&tank_elongation>1 
            h_cB10 = 23 * (materialparameters.K/materialparameters.S) - overpressure;
            thickness_wall_c = (materialparameters.c1 + materialparameters.c2 + (innerdiameter * overpressure)/h_cB10)/(1-(2 * overpressure)/h_cB10); % in m
        end
        % minimum wall thickness:
        if thickness_wall_s < materialparameters.thickness_wall_minimum % Testing weather the calculated wall thickness is smaller than allowed
           thickness_wall_s = materialparameters.thickness_wall_minimum;
        end
        if thickness_wall_c < materialparameters.thickness_wall_minimum % Testing weather the calculated wall thickness is smaller than allowed
           thickness_wall_c = materialparameters.thickness_wall_minimum;
        end
    case 2 % GRP materials:
        if tank_design == 1 % cylinder
        % Calculations after N1, circumferencial direction:
        h1 = 20 * (materialparameters.K_circum/(materialparameters.A * materialparameters.S));
        thickness_wall_circum = ((innerdiameter * overpressure)/h1)/(1-(2 * overpressure)/h1); % in m
        % Calculations after N1, longitudinal direction:
        h2 = 40 * (materialparameters.K_long/(materialparameters.A * materialparameters.S));
        thickness_wall_long = ((innerdiameter * overpressure)/h2)/(1-(2 * overpressure)/h2); % in m
        else % sphere
        % Calculations after N1, circumferencial direction:
        h1 = 40 * (materialparameters.K_circum/(materialparameters.A * materialparameters.S));
        thickness_wall_circum = ((innerdiameter * overpressure)/h1)/(1-(2 * overpressure)/h1);% in m
        % Calculations after N1, longitudinal direction:
        h2 = 40 * (materialparameters.K_long/(materialparameters.A * materialparameters.S));
        thickness_wall_long = ((innerdiameter * overpressure)/h2)/(1-(2 * overpressure)/h2); % in m
        end
        if thickness_wall_circum>thickness_wall_long
            thickness_wall_s = thickness_wall_circum;
        else
            thickness_wall_s = thickness_wall_long;
        end
        thickness_wall_c = thickness_wall_s;
        if thickness_wall_c < materialparameters.thickness_wall_minimum
            thickness_wall_s = materialparameters.thickness_wall_minimum;
            thickness_wall_c = thickness_wall_s;
        end
end