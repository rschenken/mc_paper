% Load flight profiles and store data in matrices
% hist: histogram data; time: time resolved data
% Author: Ulrich Roemer

data_shift = 0;

%% A320 (hist)
load("20182508_A320_CTred.mat")

N = 100;    % sample size (number of flight profiles)
c_fuel2power = 11.1*3.6;    % converts the fule consumption into power in MW
Fuel = data.Fuel;    % contains fuel consumption for 100 different aircraft

% determine maximum power needed over all flight profiles
P_max = 0;
for i = 1:N
    [~,P_vals] = histcounts(Fuel{i}*c_fuel2power);    % P_vals specifies locations on P-axis
    P_max = max(P_max,max(P_vals));    % get new maximum power
end
P_max = round(P_max,-2);    % round to multiple of 10

% transform data into 2D-array containing P-t Diagrams for each flight
data_hist = zeros(P_max,N);
for i = 1:N
   [t_vals,P_vals] = histcounts(Fuel{i}*c_fuel2power);
   data_hist(P_vals(2:end),i) = t_vals;    % use right boundaries of bins as locations of P-values on axis
end
X = data_hist';
X = X + data_shift*ones(N,P_max);
save('raw_data_hist_A320.mat','X')

%% B772 (hist)
load("20182508_B772_CTred.mat")

N = 100;    % sample size (number of flight profiles)
c_fuel2power = 11.1*3.6;    % converts the fule consumption into power in MW
Fuel = data.Fuel;    % contains fuel consumption for 100 different aircraft

% determine maximum power needed over all flight profiles
P_max = 0;
for i = 1:N
    [~,P_vals] = histcounts(Fuel{i}*c_fuel2power);    % P_vals specifies locations on P-axis
    P_max = max(P_max,max(P_vals));    % get new maximum power
end
P_max = round(P_max,-2);    % round to multiple of 10

% transform data into 2D-array containing P-t Diagrams for each flight
data_hist = zeros(P_max,N);
for i = 1:N
   [t_vals,P_vals] = histcounts(Fuel{i}*c_fuel2power);
   data_hist(P_vals(2:end),i) = t_vals;    % use right boundaries of bins as locations of P-values on axis
end
X = data_hist';
X = X + data_shift*ones(N,P_max);
save('raw_data_hist_B772.mat','X')

%% A320 (time)
% Read in and format data
load("20182508_A320_CTred.mat")

N = 100;    % sample size (number of flight profiles)
c_fuel2power = 11.1*3.6;    % converts the fule consumption into power in MW
Fuel = data.Fuel;    % contains fuel consumption for 100 different aircraft

% determine maximum time of flight profiles
max_t = 0;
for i = 1:N     
    max_t = max(max_t,length(Fuel{i}));    % get new maximum time 
end

% transform data into 2D-array containing P-t Diagrams for each flight
data_P_over_t = zeros(max_t,N);
for i = 1:N
    data_P_over_t(1:length(Fuel{i}),i) = Fuel{i}*c_fuel2power;      
end

X = data_P_over_t';
save('raw_data_time_A320.mat','X')

%% B772 (time)
load("20182508_B772_CTred.mat")

N = 100;    % sample size (number of flight profiles)
c_fuel2power = 11.1*3.6;    % converts the fule consumption into power in MW
Fuel = data.Fuel;    % contains fuel consumption for 100 different aircraft

% determine maximum time of flight profiles
max_t = 0;
for i = 1:N     
    max_t = max(max_t,length(Fuel{i}));    % get new maximum time 
end

% transform data into 2D-array containing P-t Diagrams for each flight
data_P_over_t = zeros(max_t,N);
for i = 1:N
    data_P_over_t(1:length(Fuel{i}),i) = Fuel{i}*c_fuel2power;      
end

X = data_P_over_t';
save('raw_data_time_B772.mat','X')

%% Air speed
load("20182508_B772_CTred_v2.mat")

N = 100;    % sample size (number of flight profiles)
v = data.TAS;    % contains fuel consumption for 100 different aircraft

% determine maximum time of flight profiles
max_t = 0;
for i = 1:N     
    max_t = max(max_t,length(v{i}));    % get new maximum time 
end

% transform data into 2D-array containing P-t Diagrams for each flight
data_v_over_t = zeros(max_t,N);
for i = 1:N
    data_v_over_t(1:length(v{i}),i) = v{i};      
end

X = data_v_over_t';
save('raw_data_v_B772.mat','X')

load("20182508_A320_CTred_v2.mat")

N = 100;    % sample size (number of flight profiles)
v = data.TAS;    % contains fuel consumption for 100 different aircraft

% determine maximum time of flight profiles
max_t = 0;
for i = 1:N     
    max_t = max(max_t,length(v{i}));    % get new maximum time 
end

% transform data into 2D-array containing P-t Diagrams for each flight
data_v_over_t = zeros(max_t,N);
for i = 1:N
    data_v_over_t(1:length(v{i}),i) = v{i};      
end

X = data_v_over_t';
save('raw_data_v_A320.mat','X')
