% Function to be executed; load results from Cholesky decomposition / PCA and generate random samples

close all, clear all;

%% load simulation data

load('181121_A320_hybridization_PCA.mat');
% load('181115_B772_hybridization_PCA.mat');

%% re-specify parameters 

% gas turbine parameters
p.eta_GT = 0.35;

% fuel cell parameters
% p.P_sp_FC = 1.6;       % [W/g] power density FC stack, based on Toyota Mirai, 2kW/kg
% p.P_sp_FC = 8.000;      % [W/g] power density FC stack, future estimate from Kadyk,Energies 2017

% battery parameters
% p.E_spBat = 0.5*3600; % [Ws/g] energy density battery; 
p.PE_Bat = 2/3600; % [kW/kWs]

% % battery parameters from Nagata16: all solid state LiS battery
% p.E_spBat = 0.5*3600; % [Ws/g] energy density battery; est: 0.5kWh/kg von Astrid/JanMueller18 Energies 
% p.PE_Bat = 2/3600; % [kW/kWs]

% alt: supercap parameters (estimates from internet search)
% p.E_spBat = 0.05*3600; % [Ws/g] 
% p.PE_Bat = 10/0.05/3600; % [kW/kWs]

figure(71);
histogram(Pmax_el./p.P_sp_FC,'Normalization','probability');
xlabel('fuel cell mass / t');

%% hybridization
%% Now let's look at undersizing the fuel cell and complimenting it with a second energy storage
% idea: cut off the "tail" of the histogram and provide that part by a battery
% i.e. "undersize" the fuel cell

% m_Batu = E_req_bat./p.E_spBat;

% m_FCu = zeros(length(Pv),n_s); %vector pre-allocation
% for lv4=1:n_s
%   m_FCu(:,lv4) = hyb(:,lv2).*(Pmax_s(lv2)*1e6./p.P_sp_FC)*p.eta_GT;
% end;

%% calculation of the total mass
% note: tank_mass/m_Tu is the overall mass of the tank system incl. hydrogen mass!
% m_tot = m_H2u+m_Batu+m_FCu+m_Tu;
% m_tot = m_Batu+m_FCu+m_Tu;


%% find the optimum = lowest system mass

% for lv4=1:n_s
%   [m_opt(lv4),i_opt(lv4)] = min(m_tot(:,lv4));
%   hyb_opt(lv4) = hyb(i_opt(lv4),lv4);
% end
% % 
% %   [m_opt,i_opt] = min(m_tot,[],1);
% % %   hyb_opt = hyb(i_opt);
% % 
% % for lv4=1:n_s
% %   hyb_opt(lv4) = hyb(i_opt(lv4),lv4);
% % end

% save(strcat('181102_',str_id,'_hybridization_tankmap_PCA.mat'));


% 
% for lv4=1:n_s
%   m_Bat_opt(lv4) = m_Batu(i_opt(lv4),lv4);
%   m_FC_opt(lv4) = m_FCu(i_opt(lv4),lv4);
%   m_H2_opt(lv4) = m_H2u(i_opt(lv4),lv4);
%   m_T_opt(lv4) = m_Tu(i_opt(lv4),lv4);
% end


%% plot figures
% % % figure(8);
% % % 
% % % subplot(2,1,1);
% % % histogram(m_opt,'Normalization','probability');
% % % xlabel('total system mass /g')
% % % ylabel('rel. #')
% % % title('energy density battery = 1 kWh/kg, power density of FC = 1.6kW/kg');
% % % subplot(2,1,2);
% % % histogram(hyb_opt,'Normalization','probability');
% % % xlabel('degree of hybridization (P_{fc}/P_{fc+bat})')
% % % ylabel('rel. #')
% % % 
% % % 
% % % figure(81);
% % % plot(hyb_opt,'x');
% % % 
% % % sample=112;
% % % 
% % % figure(82);hold on;
% % % plot(Pv_cH2,m_tot(:,sample));
% % % plot(Pv_cH2,m_Batu(:,sample));
% % % plot(Pv_cH2,m_FCu(:,sample));
% % % plot(Pv_cH2,m_H2u(:,sample));
% % % plot(Pv_cH2,m_Tu(:,sample));
% % % 
% % % 
% % % figure(83);hold on;
% % % plot(Pv_el,m_tot(:,sample));
% % % plot(Pv_el,m_Batu(:,sample));
% % % plot(Pv_el,m_FCu(:,sample));
% % % plot(Pv_el,m_H2u(:,sample));
% % % plot(Pv_el,m_Tu(:,sample));
% % % 
% % % 
% % % figure(90);
% % % histogram(m_opt,'Normalization','probability');
% % % xlabel('battery mass /g');ylabel('rel. occurance');
% % % 
% % % figure(91);
% % % histogram(m_Bat_opt,'Normalization','probability');
% % % xlabel('battery mass /g');ylabel('rel. occurance');
% % % 
% % % figure(92);
% % % histogram(m_FC_opt,'Normalization','probability');
% % % xlabel('fuel cell mass /g');ylabel('rel. occurance');
% % % 
% % % figure(93);
% % % histogram(m_H2_opt,'Normalization','probability');
% % % xlabel('hydrogen mass /g');ylabel('rel. occurance');
% % % 
% % % figure(94);
% % % histogram(m_T_opt,'Normalization','probability');
% % % xlabel('tank system mass /g');ylabel('rel. occurance');

%% parameter study

load samples_2para_10000.mat

E_spBat_v = [0.5:0.05:2]*3600;

m_edges = linspace(0.5e7,2.5e7,101); % A320
% m_edges = linspace(3e7,6.5e7,101); % B772
h_edges = linspace(0,1,101);

% for lv5=1:length(samples);
for lv5=1:200;
  E_spBat = samples(lv5,1); % [Ws/g] energy density battery; 
  m_Batu = E_req_bat./E_spBat;
  m_FCu = m_FCu * p.P_sp_FC/samples(lv5,2);  
  m_tot = m_Batu+m_FCu+m_Tu;
  for lv4=1:n_s
    [m_opt(lv4),i_opt(lv4)] = min(m_tot(:,lv4));
    hyb_opt(lv4) = hyb(i_opt(lv4),lv4);
  end
%   if lv5==1
%     [m_map(1,:) m_edges] = histcounts(m_opt,'Normalization','probability');
%     [h_map(1,:) h_edges] = histcounts(hyb_opt,h_edges,'Normalization','probability');
%   else
%     [m_map(lv5,:) m_edges] = histcounts(m_opt,m_edges,'Normalization','probability');
%     [h_map(lv5,:) h_edges] = histcounts(hyb_opt,h_edges,'Normalization','probability');
%   end

    [m_map(lv5,:), m_edges] = histcounts(m_opt,m_edges,'Normalization','probability');
    [h_map(lv5,:), h_edges] = histcounts(hyb_opt,h_edges,'Normalization','probability');

end;

tic;toc;


figure(666);surf((m_edges(2:end)+ m_edges(1:end-1))/2,E_spBat_v/3600,m_map);caxis([0 0.02]);
xlabel('system mass / g');ylabel('energy density battery / (kWh/kg)');zlabel('relative occurance');


figure(667);surf((h_edges(2:end)+ h_edges(1:end-1))/2,E_spBat_v/3600,h_map);caxis([0 0.2]);
xlabel('degree of hybridization / P_{fc}/P_{tot}');ylabel('energy density battery / (kWh/kg)');zlabel('relative occurance');
























