function m = Insulationmaterial_parameters(material)
% This function returns a data set of material parameters in dependency of 
% the chosen material. 

% Unities of the in- and output parameters:
%   d: density                   in kg/m^3
%   ht: thermal conductivity     in W/Km
%   material: number corresponding to table A.G. in the appendix
switch material
    case 1 % polyurethane:
        m.d = 32; % kg/m^3
        m.ht = 0.025; % W/Km    
    case 2 % Rohacell closed cell polymethacrylimide:
        m.d = 51.1;
        m.ht = 0.022;  
    case 11 % polyimide:
        m.d = 35;
        m.ht = 0.1;
end